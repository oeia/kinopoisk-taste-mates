import requests
from bs4 import BeautifulSoup
import time
import tkinter
from tkinter import *

HEADERS = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0}', 'accept': '*/*'}

#globals:
user_1_nickname = ''
user_2_nickname = ''
movies_list_1 = []
movies_list_1_joined = []
movies_list_1_titles = []
movies_list_2 = []
movies_list_2_joined = []
movies_list_2_titles = []
page_number_1 = 1
page_number_2 = 1

# response 200
def get_html(url, params=None):
    r = requests.get(url, headers=HEADERS, params=params)
    return r

def get_nickname_1(html):
    soup = BeautifulSoup(html, 'html.parser')
    global user_1_nickname
    time.sleep(2)  # здесь и далее: Чтобы Кинопоиск меньше сопротивлялся
    user_1_nickname = soup.find('title').get_text().replace('Профиль: ', '').replace(' - Оценки', '')
    if user_1_nickname == 'ÐÐ¹!': # Кинопоиск не всегда хочет делится информацией
        i_tried_times = 1
        while i_tried_times < 10:
            print(".")
            time.sleep(2)
            user_1_nickname = soup.find('title').get_text().replace('Профиль: ', '').replace(' - Оценки', '')
            i_tried_times = i_tried_times + 1
            if user_1_nickname != 'ÐÐ¹!':
                break
        if i_tried_times == 10:
            user_1_nickname = "user" + user_1_id

    print(user_1_nickname)
    return user_1_nickname
    time.sleep(2)

def get_nickname_2(html):
    soup = BeautifulSoup(html, 'html.parser')
    global user_2_nickname
    time.sleep(2)
    user_2_nickname = soup.find('title').get_text().replace('Профиль: ', '').replace(' - Оценки', '')
    if user_2_nickname == 'ÐÐ¹!':
        i_tried_times = 1
        while i_tried_times < 10:
            print(".")
            time.sleep(2)
            user_1_nickname = soup.find('title').get_text().replace('Профиль: ', '').replace(' - Оценки', '')
            i_tried_times = i_tried_times + 1
            if user_2_nickname != 'ÐÐ¹!':
                break
        if i_tried_times == 10:
            user_2_nickname = "user" + user_2_id
        
            
    print(user_2_nickname)
    return user_2_nickname
    time.sleep(2)

def get_content_1(html):
    soup = BeautifulSoup(html, 'html.parser') 
    movies = soup.find_all('div', class_='item')
    global movies_list_1
    movies_list_1 = []
    for movie in movies:
        movies_list_1.append ({
        'movie_title':movie.find(class_='nameRus').get_text(),
        'rate':movie.find(class_='vote').get_text()
        })
    return movies_list_1

def get_content_2(html):
    soup = BeautifulSoup(html, 'html.parser')
    movies = soup.find_all('div', class_='item')
    global movies_list_2
    movies_list_2 = []
    for movie in movies:
        movies_list_2.append ({
        'movie_title':movie.find(class_='nameRus').get_text(),
        'rate':movie.find(class_='vote').get_text()
        })
    return movies_list_2

def stalk_user_1():
    global user_1_id
    user_1_id = str(user_1_id)
    URL = (''.join("https://www.kinopoisk.ru/user/" + user_1_id + "/votes/list/ord/rating/#list"))
    html = get_html(URL)
    time.sleep(2)
    get_nickname_1(html.text)
    
    global page_number_1
    page_number_string_1 = str(page_number_1)
    while page_number_1 <= 50:
        page_number_string_1 = str(page_number_1)
        URL = (''.join("https://www.kinopoisk.ru/user/" + user_1_id + "/votes/list/ord/rating/page/" + page_number_string_1 + "/#list"))
        html = get_html(URL)
        time.sleep(2)
        get_content_1(html.text)
        kino_1 = get_content_1(html.text)

        # Если с первого раза не считалось
        while kino_1 == []:
            print("\nКинопоиск не хочет делиться информацией! Затаимся на пару минут и попробуем еще раз.")
            t = 1
            while t < 61:
                time.sleep(2)
                print('.', end = '') # Подаёт признаки жизни
                t = t + 1
                if t == 61:
                    print('.', end = '') # Подаёт признаки жизни
            html = get_html(URL)
            get_content_1(html.text)
            kino_1 = get_content_1(html.text)

        print(" На шаг ближе к цели! ", end = '')
        for i in reversed(range(len(movies_list_1))): #Убираем фильмы с оценкой 6 и ниже
            if movies_list_1[i]['rate'] == '6' \
            or movies_list_1[i]['rate'] == '5' \
            or movies_list_1[i]['rate'] == '4' \
            or movies_list_1[i]['rate'] == '3' \
            or movies_list_1[i]['rate'] == '2' \
            or movies_list_1[i]['rate'] == '1':
                del movies_list_1[i]
        movies_list_1_joined.extend(movies_list_1)
        time.sleep(2)
        page_number_1 = page_number_1 + 1
        if len(movies_list_1) < 50: # Если добрались до шестерок (и убрали их),
            break                   # дальше листать смысла нет

    for ii in movies_list_1_joined: # Теперь оценки уже не важны, уберем их
        del ii['rate']
    global movies_list_1_titles
    for iii in movies_list_1_joined:
        for v in iii.values():
            movies_list_1_titles.append(v)
    print(" Первый пользователь готов!")
    return movies_list_1_titles

def stalk_user_2(): # Со вторым пользователем проделывает все то же самое
    global user_2_id
    user_2_id = str(user_2_id)
    URL = (''.join("https://www.kinopoisk.ru/user/" + user_2_id + "/votes/list/ord/rating/#list"))
    html = get_html(URL)
    time.sleep(2)
    get_nickname_2(html.text)
    
    global page_number_2
    page_number_string_2 = str(page_number_2)
    while page_number_2 <= 50:
        page_number_string_2 = str(page_number_2)
        URL = (''.join("https://www.kinopoisk.ru/user/" + user_2_id + "/votes/list/ord/rating/page/" + page_number_string_2 + "/#list"))
        html = get_html(URL)
        time.sleep(2)
        get_content_2(html.text)
        kino_2 = get_content_2(html.text)

        while kino_2 == []:
            print("\nКинопоиск не хочет делиться информацией! Затаимся на пару минут и попробуем еще раз.")
            t = 1
            while t < 61:
                time.sleep(2)
                print('.', end = '')
                t = t + 1
                if t == 61:
                    print('.', end = '')
            html = get_html(URL)
            get_content_2(html.text)
            kino_2 = get_content_2(html.text)

        print(" На шаг ближе к цели! ", end = '')
        for i in reversed(range(len(movies_list_2))):
            if movies_list_2[i]['rate'] == '6' \
            or movies_list_2[i]['rate'] == '5' \
            or movies_list_2[i]['rate'] == '4' \
            or movies_list_2[i]['rate'] == '3' \
            or movies_list_2[i]['rate'] == '2' \
            or movies_list_2[i]['rate'] == '1':
                del movies_list_2[i]
        movies_list_2_joined.extend(movies_list_2)
        time.sleep(2)
        page_number_2 = page_number_2 + 1
        if len(movies_list_2) < 50:
            break
    for ii in movies_list_2_joined:
        del ii['rate']
    global movies_list_1_titles
    for iii in movies_list_2_joined:
        for v in iii.values():
            movies_list_2_titles.append(v)
    print(" Второй пользователь готов! ", end = '')
    return movies_list_2_titles

def find_common_movies():
    global movies_list_1_titles
    global user_1_nickname
    print("\n", user_1_nickname, "высоко оценил(а):")
    print(movies_list_1_titles)
    global movies_list_2_titles
    global user_2_nickname
    print("\n", user_2_nickname, "высоко оценил(а):")
    print(movies_list_2_titles)
    kino_both = []
    for m in movies_list_1_titles:
        for n in movies_list_2_titles:
            if m == n:
                kino_both.append(m)
                break
    if kino_both == []:
        print("У этих пользователей совсем разные вкусы!")
    else:
        with open('movies_ive_seen.txt', 'r', encoding="utf-8") as file:
            seen = list(file)
            kino_both = list(set(kino_both) - set(seen))
        print("\nОба пользователя высоко оценили: " )
        print(kino_both)
        file.close()

# Выбрать уже просмотренные фильмы, чтобы их не показывать
    class CheckButton:
        def __init__(self, master, title):
            self.var = IntVar()
            self.var.set(0)
            self.title = title
            self.cb = Checkbutton(master, text=title, variable=self.var, onvalue=1, offvalue=0)
            self.cb.pack(side = TOP)

    def i_have_seen():
        movies_i_have_seen = []
        for i in checks:
            if i.var.get():
                movies_i_have_seen.append(i.title)
        create_or_open_file = open('movies_ive_seen.txt', 'a', encoding="utf-8")
        with open('movies_ive_seen.txt', 'a', encoding="utf-8") as file:
            for i in movies_i_have_seen:
                file.write(i + '\n')
        movies_to_see = list(set(kino_both) - set(movies_i_have_seen))
        print("Из того, что Вы не смотрели, оба пользователя высоко оценили:")
        print(movies_to_see)
        file.close()

    #Диалоговое окно
    window = Tk()
    window.title("Какие из этих фильмов вы уже видели?")
    window.geometry('600x800')

    #scrollbar
    scroll = Scrollbar(window)
    canvas = Canvas(window, height = 700, width = 500, yscrollcommand=scroll.set)
    canvas.config(scrollregion=canvas.bbox("all"))
    scroll.config(command=canvas.yview)
    scroll.pack(side="right",fill="y")

    f1 = Frame(canvas)
    canvas.pack(padx=10, pady=10)
    canvas.create_window(250, 0, window = f1)
    checks = []
    for i in kino_both:
        checks.append(CheckButton(f1, i))
    window.update() 
    canvas.config(scrollregion=canvas.bbox("all"))
    f2 = Frame()
    f2.pack(side ="bottom")
    button = Button(f2, text="Отметить выбранное как просмотренное", command=i_have_seen)
    button.pack(side ="bottom")
    window.mainloop()

user_1_id = int(input("Введите ID первого пользователя (число): "))
user_2_id = int(input("Введите ID другого пользователя (число): "))
stalk_user_1()
stalk_user_2()
find_common_movies()
